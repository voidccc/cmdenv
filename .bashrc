alias v='vim'
alias c='cd ..'
alias ll='ls -alh'
alias grep='grep --color=auto'
alias m='make 2>&1 | color'
alias lg="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ad)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias gitp='git push -u origin master'
