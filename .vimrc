set nocompatible
source $VIMRUNTIME/vimrc_example.vim
" source $VIMRUNTIME/mswin.vim
" behave mswin

"set diffexpr=MyDiff()
"function! MyDiff()
"  let opt = '-a --binary '
"  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
"  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
"  let arg1 = v:fname_in
"  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
"  let arg2 = v:fname_new
"  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
"  let arg3 = v:fname_out
"  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
"  let eq = ''
"  if $VIMRUNTIME =~ ' '
"    if &sh =~ '\<cmd'
"      let cmd = '""' . $VIMRUNTIME . '\diff"'
"      let eq = '"'
"    else
"      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
"    endif
"  else
"    let cmd = $VIMRUNTIME . '\diff'
"  endif
"  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
"endfunction
"****************************************************
"  Created WangGang 2010.5.7
"  Last Update 2011.12.14
"  GNU on Windows Platform http://gnuwin32.sourceforge.net/
"****************************************************
"  nmap : key map on normal-mode 
"  <CR> : enter key
"****************************************************
"let workpath = 'Backup_br_Gingerbread_7x30'
let current = $VIM_CURRENT
let root = $VIM_ROOT
let ctagsfile='vimuse_ctags'
let filename='/vimuse_filenametags'

let mapleader = ','             "use ',' as <leader>
set fenc=gbk                    "GBK
set nobackup                    "no backup file(file.swp)
set noswapfile                  "no swap file (~file)
syntax on                       "syntax with color
"set nu                          "set num
set noeb                        "no sound when error happy
set guioptions-=m               "no GUI menu( += to show)
set guioptions-=T               "no GUI toolbox
set guioptions-=l               "no GUI scroll bar on left(r is right)
colorscheme slate                "the scheme is ron 
set shiftwidth=4
set tabstop=4
set expandtab                   "use space replace tab.
filetype plugin on

" ctags
"set tags=$VIM_CURRENT/vimuse_ctags        "Ctags path
set tags=~/code/netlib4/vimuse_ctags        "Ctags path
set autochdir                            "Ctags
"Without environment values we must config the real path of it as
"Tlist_Ctags_Cmd = '~/../../../ctags'
let Tlist_Ctags_Cmd = 'ctags'
let Tlist_Show_One_File=1       "Taglist
let Tlist_Exit_OnlyWindow=1     "Taglist
nmap <C-\> :ts<cr>              "Ctags show define list

" LookupFile
let g:LookupFile_TagExpr = string(current . filename)
let g:LookupFile_PreserveLastPattern = 0 " don't save last word
let g:LookupFile_AlwaysAcceptFirst = 1 " default open the first
let g:LookupFile_AllowNewFiles = 0 " not allow create new file when one not exist

" JSearch
"let g:JSearch_path = getcwd()

" Hexadecimal system
let s:hexModle = "N"
function! ToHexModle()
  if s:hexModle == "Y"
    %!xxd -r
    let s:hexModle = "N"
  else
    %!xxd
    let s:hexModle = "Y"
  endif
endfunction
" key map of hex
map <leader>h :call ToHexModle()<cr>

" mouse mode
let s:mouseMode = "N"
function! ToMouseMode()
  if s:mouseMode == "Y"
    set mouse=
    let s:mouseMode = "N"
  else
    set mouse=a
    let s:mouseMode = "Y"
  endif
endfunction
" key map of mouse mode 
map <F2> :call ToMouseMode()<cr>

" ConqueTerm (open cmd in vim)
let s:conquetermMode = "N"
function! ToConqueTermMode()
  if s:conquetermMode == "Y"
    :q
    let s:conquetermMode = "N"
  else
    ConqueTermSplit bash
    let s:conquetermMode = "Y"
  endif
endfunction
" key map of mouse mode 
map <F4> :call ToConqueTermMode()<cr>

" SuperTab
" let g:SuperTabRetainCompletionType=2
" let g:SuperTabDefaultCompletionType="<C-X><C-O>"

" split window
" Ctrl + wh (up-down, window-horizontal)
" Ctrl + wv (left-right, window-vertical)
" <C-s> is the pause key on terminal, so I change it to <C-h>
"nnoremap <C-w><C-v> :vsp<CR>
nnoremap <C-w><C-u> :sp<CR>

" filtering plugin
" use a to open/close auto-jump, I like it open
nnoremap ,f :call FilteringNew().addToParameter('alt', @/).run()<CR>
" nnoremap ,F :call FilteringNew().parseQuery(input('>'), '|').run()<CR>
nnoremap ,g :call FilteringGetForSource().return()<CR>

" Run Python
map <F12> :!start cmd /k python %<cr>

" auto complete
filetype plugin indent on
set completeopt=longest,menu
" menu style on auto complete
set wildmenu
" language about auto compete
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType java set omnifunc=javacomplete#Complete

" BufExplorer
let g:bufExplorerDefaultHelp=0       " Do not show default help.
let g:bufExplorerSplitRight=0        " Split left.
let g:bufExplorerSplitVertical=1     " Split vertically.
let g:bufExplorerSplitVertSize = 30  " Split width
let g:bufExplorerUseCurrentWindow=1  " Open in new window.
autocmd BufWinEnter \[Buf\ List\] setl nonumber

" winManager
let g:winManagerWindowLayout = "BufExplorer,FileExplorer|TagList"
let g:winManagerWidth = 30
let g:defaultExplorer = 0
nmap <silent> <F8> :WMToggle<cr>

"Ha ha ha
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

"use <leader>ev to quick open .vimrc
nnoremap <leader>ev <C-w><C-v>:e $MYVIMRC<cr>
nnoremap <leader>re <C-w><C-v>:e /mnt/disk/huawei/register.txt<cr>

"Shell in vim
nnoremap <leader>sh <C-w><C-l>:ConqueTerm bash<cr>

au BufEnter *.txt setlocal ft=txt

map <leader>c :ColorSchemeExplorer<cr>

"A plugin
":AS up-down subwindow
":AV left-right subwindow
":IH jump into the #include file

" use :cw to open quickfix window
